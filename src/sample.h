
class MyClass {
public:
	MyClass();
	~MyClass();

	void doSometing();

private:
	bool subRoutine( int state );

	int m_classState;
};
