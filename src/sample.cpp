#include <stdio.h>
#include "sample.h"

void main()
{
	MyClass myClass;
	myClass.doSometing();
}

MyClass::MyClass()
	: m_classState(0)
{
}

MyClass::~MyClass()
{
}

void MyClass::doSometing()
{
	if( ! subRoutine( 1 ) ) {
		printf( "failed.¥n" );
	}
}

bool MyClass::subRoutine( int state )
{
	if( 0 == state ) {
		return false;
	}

	m_classState = state;
	return true;
}
